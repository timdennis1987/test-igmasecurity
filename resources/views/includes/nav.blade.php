<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <a class="navbar-brand" href="/">
        {{ $companyName->description }}
    </a>
    <button class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="#history-section">History</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#people-section">People</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#service-section">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#careers-section">Careers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact-section">Contact Us</a>
            </li>
        </ul>
    </div>
</nav>
