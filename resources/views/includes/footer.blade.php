<footer>
    <div class="container">
        <div class="row">
            <div class="mx-auto">
                A <a href="http://pixlquick.com/">Pixlquick</a> website - {{ date('Y') }}
            </div>
        </div>
    </div>
</footer>
