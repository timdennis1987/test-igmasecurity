<div class="row mb-5">
    <div id="people" class="mx-auto text-center">
        <i class="fas fa-users fa-3x mb-3"></i>
        <h1>People</h1>
    </div>
</div>

<div class="row">

    <p class="lead-text text-center">
        How we look after our people plays a crucial role in the success of our operations and our business as a whole.
        <br>
        As part of our people strategy we have implemented new ideas and benefits, designed new CSR programmes to help improve employee engagement.
    </p>

    <div class="mx-auto mt-5 mb-3">
        <h3>Health & Safety</h3>
    </div>

    <p class="lead-text text-center">
        Igma Security is fully committed to ensuring the health, safety and wellbeing of our employees and anyone who comes into contact with our services.
        <br>
        In this regard, all aspects of our day-to-day business operations are conducted in full compliance with health and safety legislation and best practice pertaining to health and safety in the workplace.
    </p>

</div>
