<div class="hero-padding">
    <div class="container">
        <div class="row">
            <div class="col text-center text-white">
                <h1 class="page-title">
                    {{ $companyName->description }}
                </h1>
            </div>
        </div>
        <div class="hero-cta">
            <div class="row">
                <div class="mx-auto">
                    <a href="#contact"
                       class="btn btn-primary contact-us shadow py-3 px-5">
                        Contact us for a <strong>FREE</strong> quote
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
