<div class="row mb-5">
    <div id="services" class="mx-auto text-center text-white">
        <i class="fas fa-shield-alt fa-3x mb-3"></i>
        <h1>Services.</h1>
    </div>
</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <img src="/img/services/manned-guarding.jpg" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-md-last my-auto">
        <div class="service-text-right">
            <h3>
                {{ $mannedGuarding->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $mannedGuarding->description !!}
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-last my-auto">
        <img src="/img/services/event-security.jpg" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <div class="service-text-left">
            <h3>
                {{ $eventSecurity->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $eventSecurity->description !!}
                </p>
            </div>
        </div>
    </div>

</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <img src="/img/services/crowd-management.png" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-lg-last my-auto">
        <div class="service-text-right">
            <h3>
                {{ $crowdManagement->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $crowdManagement->description !!}
                </p>
            </div>
        </div>
    </div>

</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-last my-auto">
        <img src="/img/services/front-of-house.jpg" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <div class="service-text-left">
            <h3>
                {{ $frontOfHouse->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $frontOfHouse->description !!}
                </p>
            </div>
        </div>
    </div>

</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <img src="/img/services/mobile-patrols.jpg" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-lg-last my-auto">
        <div class="service-text-right">
            <h3>
                {{ $mobilePatrols->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $mobilePatrols->description !!}
                </p>
            </div>
        </div>
    </div>

</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-last my-auto">
        <img src="/img/services/construction-site-security2.png" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <div class="service-text-left">
            <h3>
                {{ $constructionSite->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $constructionSite->description !!}
                </p>
            </div>
        </div>
    </div>

</div>

<div class="row services-row">

    <div class="col-md-12 col-lg-6 order-lg-first my-auto">
        <img src="/img/services/cctv-monitoring.jpg" width="100%">
    </div>

    <div class="col-md-12 col-lg-6 order-lg-last my-auto">
        <div class="service-text-right">
            <h3>
                {{ $cctvMonitoring->title }}
            </h3>

            <div class="lead-text">
                <p>
                    {!! $cctvMonitoring->description !!}
                </p>
            </div>
        </div>
    </div>

</div>
