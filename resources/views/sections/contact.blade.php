<div class="row mb-5">
    <div id="contact" class="mx-auto text-center">
        <i class="far fa-address-card fa-3x mb-3"></i>
        <h1>Contact Us</h1>
    </div>
</div>

<div class="contact-details">
    @foreach($contactType as $contact)
        <h4>
            <i class="{{ $contact->icon }}"></i> :
            <a href="{{ $contact->class }}:{{ $contact->description }}"
               target="_blank">
                {{ $contact->description }}
            </a>
        </h4>
    @endforeach

    <h4>
        <i class="fas fa-building"></i> : 5-10 St Paul's Churchyard, London, EC4M 8AL
    </h4>
</div>

<p class="mt-5">
    Fill out the form below and we will get back to you as soon as possible
</p>

<form>
    <div class="row">
        <div class="col-md-6 my-3">
            <input type="text" class="form-control" placeholder="First name">
        </div>
        <div class="col-md-6 my-3">
            <input type="text" class="form-control" placeholder="Last name">
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 my-3">
            <input type="email" class="form-control" placeholder="Email address">
        </div>
        <div class="col-md-6 my-3">
            <input type="tel" class="form-control" placeholder="Phone number">
        </div>
    </div>

    <div class="form-group">
        <textarea class="form-control my-3" id="message" placeholder="Add message..." rows="3"></textarea>
    </div>

    <button class="btn btn-success my-3">Send message</button>
</form>
