<div class="row mb-5">
    <div id="services" class="mx-auto text-center text-white">
        <i class="fas fa-briefcase fa-3x mb-3"></i>
        <h1>Career Advice</h1>
    </div>
</div>

<div class="row">

    <p class="lead-text text-center text-white">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Aliquam autem culpa fuga fugiat fugit in natus nemo, neque odit omnis quasi quis rem, reprehenderit,
        repudiandae sed veniam voluptates? Blanditiis, voluptatibus.
    </p>

</div>

<div class="row mt-5">

    <div class="mx-auto">
        <a href="mailto:recruitment@igmasecurityservices.co.uk" class="btn btn-primary shadow py-3 px-5">Get Advice</a>
    </div>

</div>
