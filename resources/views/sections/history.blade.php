<div class="row mb-5">
    <div id="history" class="mx-auto text-center text-white">
        <i class="fas fa-history fa-3x mb-3"></i>
        <h1>History</h1>
    </div>
</div>

<div class="row">

    <p class="lead-text text-center text-white">
        Established in 2010,
        Igma Security services has grown to become one of the UK’s leading security companies with various network across the country,
        a well-trained workforce and Licensed by the Security Industry Authority.
    </p>

</div>
