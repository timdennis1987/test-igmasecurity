@extends('layouts.app')
@include('includes.nav')
@section('content')

    <div class="section hero hero-bg">
        @include('sections.hero')
    </div>

    <section id="history-section">
        <div class="container">
            @include('sections.history')
        </div>
    </section>

    <section id="people-section">
        <div class="container">
            @include('sections.people')
        </div>
    </section>

    <section id="service-section">
        @include('sections.services')
    </section>

    <section id="careers-section">
        <div class="container">
            @include('sections.careers')
        </div>
    </section>

    <section id="contact-section">
        <div class="container">
            @include('sections.contact')
        </div>
    </section>

    @include('includes.footer')
@stop
