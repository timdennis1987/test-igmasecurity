<?php

namespace App\Http\Controllers;

use App\Option;
use App\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        $companyName = Option::where('value', 'company_name')->first();
        $contactType = Option::where('type', 1)->where('display', 1)->get();
        $mannedGuarding = Service::where('order', 1)->first();
        $eventSecurity = Service::where('order', 2)->first();
        $crowdManagement = Service::where('order', 3)->first();
        $frontOfHouse = Service::where('order', 4)->first();
        $mobilePatrols = Service::where('order', 5)->first();
        $constructionSite = Service::where('order', 6)->first();
        $cctvMonitoring = Service::where('order', 7)->first();

        return view('home', [
            'companyName' => $companyName,
            'contactType' => $contactType,
            'mannedGuarding' => $mannedGuarding,
            'eventSecurity' => $eventSecurity,
            'crowdManagement' => $crowdManagement,
            'frontOfHouse' => $frontOfHouse,
            'mobilePatrols' => $mobilePatrols,
            'constructionSite' => $constructionSite,
            'cctvMonitoring' => $cctvMonitoring,
        ]);
    }
}
